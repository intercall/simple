#ifndef QSKYPE_H
#define QSKYPE_H

#include <QObject>
#include <QtDBus/QDBusInterface>

class QSkype : public QObject
{
    Q_OBJECT
public:
    explicit QSkype(QObject *parent = 0);
    void call(QString number);
    QString Invoke(QString cmd);
private:
    QDBusInterface *_skype;
signals:
    void connected();
    void disconnected();
public slots:

};

#endif // QSKYPE_H
