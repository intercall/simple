#ifndef QATCONNECTION_H
#define QATCONNECTION_H

#include <QObject>
#include "qextserialport.h"

class QATConnection : public QObject
{
    Q_OBJECT
public:
    explicit QATConnection(QObject *parent = 0);
    bool COMwriteData(QByteArray data, QByteArray waitFor, int number, QByteArray *res);
    bool isCall() {return _isCall; }
private:
    bool _isCall;
    bool _disableEvents;
    QString _data;
    QextSerialPort port;
    void _Init();
    void _commandReceived();
public slots:
    void pickUp();
    void hangUp();
private slots:
    void dataReady();
signals:
    void ring();
};

#endif // QATCONNECTION_H
