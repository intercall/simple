#include <QDebug>
#include <QDateTime>
#include <QFile>
#include <stdlib.h>
#include "routines.h"

QFile log;

void Check(bool result, QString msg, bool critical)
{
    if (result) return;
    writeLog(msg);
    if (critical)
    {
        exit(1);
    }
}

void writeLog(QString msg)
{
    qDebug() << msg;
    log.setFileName("log.txt");
    log.close();
    log.open(QIODevice::Text | QIODevice::Append);
    log.write(QByteArray().append(QDateTime::currentDateTime().toString(Qt::ISODate).replace("T"," ")+" "+msg+"\n"));
    log.close();
}

void clearLog()
{
    QFile::remove("log.txt");
}
