#include <QDebug>
#include <QFile>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QCoreApplication::setApplicationName("InterCall");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::on_PBStart_clicked()
{
    ui->PBStart->setEnabled(false);
    _At = new QATConnection(this);
    _Speaker = new QSpeaker(this);
    connect(_At, SIGNAL(ring()), this, SLOT(ringReceived()));
    connect(_Speaker, SIGNAL(said()), this, SLOT(sayFinished()));
    connect(_Speaker, SIGNAL(listened()), this, SLOT(listened()));
}

void MainWindow::ringReceived()
{    
    _At->pickUp();
    _Speaker->sayHello();
}

void MainWindow::sayFinished()
{
//    _skype = new QSkype(this);
//    _skype->call("scintilla22");
    _Speaker->recordVoice(5);
}

void MainWindow::listened()
{
    _Speaker->say("sounds/voice.raw");
}

void MainWindow::on_pushButton_clicked()
{
    _Speaker = new QSpeaker(this);
    _Speaker->say("test.raw");
//    QFile f("sounds/voice.raw");
//    f.open(QIODevice::ReadOnly);
//    qDebug() << _Speaker->parseDTMF(&f);
//    f.close();
}
