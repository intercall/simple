#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qatconnection.h"
#include "qspeaker.h"
#include "qskype.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;
    QATConnection *_At;
    QSpeaker *_Speaker;
    QSkype *_skype;
private slots:
    void on_pushButton_clicked();
    void on_PBStart_clicked();
    void ringReceived();
    void sayFinished();
    void listened();
};

#endif // MAINWINDOW_H
