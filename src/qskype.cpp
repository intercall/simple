#include <QDebug>
#include "qskype.h"
#include <QtDBus/QDBusReply>

QSkype::QSkype(QObject *parent) : QObject(parent)
{
    _skype = new QDBusInterface("com.Skype.API", "/com/Skype", "com.Skype.API");
    if (Invoke("NAME InterCall") != "OK") return;
    if (Invoke("PROTOCOL 5") != "PROTOCOL 5") return;
}

QString QSkype::Invoke(QString cmd)
{
    qDebug() << "InterCall: " + cmd;
    QDBusReply<QString> reply = _skype->call("Invoke", cmd);
    qDebug() << "Skype: " + reply.value();
    return reply.value();
}

void QSkype::call(QString number)
{
    Invoke("#01 CALL " + number);
}
