#ifndef ROUTINES_H
#define ROUTINES_H

#include <QObject>

void Check(bool result, QString msg, bool critical = false);
void writeLog(QString msg);
void clearLog();

#endif // ROUTINES_H
