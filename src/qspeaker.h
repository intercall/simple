#ifndef QSPEAKER_H
#define QSPEAKER_H

#include <QObject>
#include <QtMultimedia/QAudioInput>
#include <QtMultimedia/QAudioOutput>
#include <QtMultimedia/QAudioFormat>
#include <QFile>
#include "DtmfDetector.hpp"

class QSpeaker : public QObject
{
    Q_OBJECT
public:
    explicit QSpeaker(QObject *parent = 0);
    ~QSpeaker();
    void sayHello(QObject *receiver, const char * callback);
    void say(QString path);
    void listenNumber();
    void recordVoice(int sec);
    QString parseDTMF(QIODevice *device);
private:
    DtmfDetector *_dtmf;
    QAudioFormat _format;
    QAudioInput *_input;
    QAudioOutput *_output;
    QFile *_outputFile;
signals:
    void said();
    void listened();
public slots:
    void sayHello();
private slots:
    void sayFinished(QAudio::State state);
    void stopRecording();
};

#endif // QSPEAKER_H
