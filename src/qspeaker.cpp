#include <QTimer>
#include <QFile>
#include <QDebug>

#include "qspeaker.h"

QSpeaker::QSpeaker(QObject *parent) : QObject(parent)
{
    _format.setSampleRate(44100);
    _format.setChannelCount(1);
    _format.setSampleSize(8);
    _format.setCodec("audio/pcm");
    _format.setByteOrder(QAudioFormat::LittleEndian);
    _format.setSampleType(QAudioFormat::UnSignedInt);

    qDebug() << _format.sampleRate() << _format.channelCount() << _format.sampleSize() << _format.codec() << _format.byteOrder() << _format.sampleType();
    QAudioDeviceInfo::availableDevices(QAudio::AudioInput)[1].preferredFormat();

    qDebug() << _format.sampleRate() << _format.channelCount() << _format.sampleSize() << _format.codec() << _format.byteOrder() << _format.sampleType();

    if (!QAudioDeviceInfo::availableDevices(QAudio::AudioInput)[1].isFormatSupported(_format) ||
        !QAudioDeviceInfo::availableDevices(QAudio::AudioOutput)[1].isFormatSupported(_format))
    {
        qWarning()<<"default format not supported try to use nearest";
    }

    _outputFile = NULL;

    foreach (QAudioDeviceInfo i, QAudioDeviceInfo::availableDevices(QAudio::AudioInput))
    {
        if (i.deviceName() == "pulse") _input = new QAudioInput(i, _format, this);
    }

    foreach (QAudioDeviceInfo i, QAudioDeviceInfo::availableDevices(QAudio::AudioOutput))
    {
        if (i.deviceName() == "pulse") _output = new QAudioOutput(i, _format, this);
    }

    connect(_output, SIGNAL(stateChanged(QAudio::State)), this, SLOT(sayFinished(QAudio::State)));

    _dtmf = new DtmfDetector(16);
}

QSpeaker::~QSpeaker()
{
    delete _dtmf;
}

void QSpeaker::sayFinished(QAudio::State state)
{
    if(state == QAudio::IdleState)
    {
        _output->stop();
        _outputFile->close();
        delete _outputFile;
        _outputFile = NULL;
        emit said();
    }
}

void QSpeaker::say(QString path)
{
    qDebug() << "saying "+path;
    if (_outputFile != NULL) return;
    if (!QFile::exists(path)) return;

    _outputFile = new QFile(path);
    _outputFile->open(QIODevice::ReadOnly);
    _output->start(_outputFile);
}

void QSpeaker::sayHello()
{
    say("sounds/test.raw");
}

void QSpeaker::listenNumber()
{
    //_inputdata = _input->start();
}

void QSpeaker::recordVoice(int sec)
{
    qDebug() << "recording";
    if (_outputFile != NULL) return;
    _outputFile = new QFile("sounds/voice.raw");
    _outputFile->open( QIODevice::WriteOnly | QIODevice::Truncate );
    QTimer::singleShot(sec*1000, this, SLOT(stopRecording()));
    _input->start(_outputFile);
}

void QSpeaker::stopRecording()
{
    _input->stop();
    _outputFile->close();
    delete _outputFile;
    _outputFile = NULL;
    emit listened();
}

QString QSpeaker::parseDTMF(QIODevice *device)
{
    QString result;

    short int samples[100000];
    char *data = (char *)samples;
    int i = 0;

    while (!device->atEnd())
    {
        char d;
        device->read(&d, 1);
        data[i++] = d;
    }


    qDebug() <<  samples[1000];

    DtmfDetector dtmf(160);
    dtmf.zerosIndexDialButton();
    dtmf.dtmfDetecting(samples);

    for(int ii = 0; ii < dtmf.getIndexDialButtons(); ++ii)
    {
        result+=QString::number(dtmf.getDialButtonsArray()[ii]);
    }
    return result;
}
