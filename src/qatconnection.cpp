#include <QDateTime>
#include "qatconnection.h"
#include "routines.h"

QATConnection::QATConnection(QObject *parent) :
    QObject(parent)
{
    _Init();
}

bool QATConnection::COMwriteData(QByteArray data, QByteArray waitFor, int number, QByteArray *res)
{
    res->clear();
    if (data.length() != 0)
    {
        writeLog("PC:  "+QString(data.trimmed()));
    }

    _disableEvents = true;
    port.write(data);
    char d = 0;
    int i = 0;
    int t = 0;
    while (number != 0)
    {
        if (++t == 100) break;
        if (port.read(&d,1) > 0)
        {
            res->append(d);
            if (d == waitFor[i]) i++; else
            {
                i = 0;
                if (d == waitFor[i]) i++;
            }
            if (i >= waitFor.length())
            {
                number--;
                i = 0;
            }
        }
    }
    _disableEvents = false;
    if (!QString(*res).trimmed().simplified().isEmpty())
    {
        writeLog("GSM: "+QString(*res).trimmed().simplified());
    }
    return number == 0;
}


void QATConnection::_Init()
{
    _disableEvents = false;
    _isCall = false;
    QByteArray Data;
    port.setPortName("/dev/rfcomm0");
    Check(port.open(QIODevice::ReadWrite),"Connect Error: can't open port", true);

    port.setDataBits(DATA_8);
    port.setParity(PAR_NONE);

    Check(COMwriteData("ATZ\r\n","\r\n", 2,&Data),"Connect Error: can't reset device", true);
    Check(COMwriteData("ATE0\r\n","\r\n", 2,&Data),"Connect Error: can't reset device", true);
    Check(COMwriteData("AT\r\n","OK\r\n", 2,&Data),"Connect Error: there is no answer from device", true);

    connect(&port, SIGNAL(readyRead()), this, SLOT(dataReady()));
}

void QATConnection::dataReady()
{
    if (_disableEvents) return;
    char d;
    while (port.read(&d,1) > 0)
    {
        _data.append(d);
        if (d == 0x0a)
        {
            _commandReceived();
            _data.clear();
        }
    }
}

void QATConnection::pickUp()
{
    QByteArray Data;
    if (isCall()) return;
    Check(COMwriteData("ATA\r\n","\r\n", 2,&Data) || !Data.contains("OK"),"Device Error: can't pick up");
    _isCall = true;
}

void QATConnection::hangUp()
{
    QByteArray Data;
    if (!isCall()) return;
    Check(COMwriteData("ATH\r\n","\r\n", 2,&Data) || !Data.contains("OK"),"Device Error: can't hang up");
}

void QATConnection::_commandReceived()
{
    writeLog("GSM: " + _data.trimmed().simplified());
    if (_data.contains("RING"))
        emit ring();
}
