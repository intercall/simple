#-------------------------------------------------
#
# Project created by QtCreator 2010-07-17T20:22:13
#
#-------------------------------------------------

QT       += core gui phonon multimedia dbus

TARGET = Simple
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    routines.cpp \
    qatconnection.cpp \
    qspeaker.cpp \
    ..\..\Libs\dtmf_cpp\DtmfDetector.cpp \
    qskype.cpp

HEADERS  += mainwindow.h \
    routines.h \
    qatconnection.h \
    qspeaker.h \
    qskype.h

FORMS    += mainwindow.ui

INCLUDEPATH += ../../Libs/qextserialport/include
INCLUDEPATH += ../../Libs/dtmf_cpp
INCLUDEPATH += /programs/Qt/qt-4.7/include
OBJECTS_DIR = tmp
MOC_DIR = tmp
UI_DIR = tmp
CONFIG(debug, debug|release):LIBS += -L../../Libs/qextserialport/lib -lqextserialport
else:LIBS += -L../../Libs/qextserialport/lib -lqextserialport
win32:LIBS += -lsetupapi

OTHER_FILES += \
    Histrory.txt

# бібліотеки COM-порта мають бути в /usr/lib
