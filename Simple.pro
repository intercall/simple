#-------------------------------------------------
#
# Project created by QtCreator 2010-07-17T20:22:13
#
#-------------------------------------------------

QT       += core gui

TARGET = Simple
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

INCLUDEPATH += qextserialport
OBJECTS_DIR = tmp
MOC_DIR = tmp
UI_DIR = tmp
CONFIG(debug, debug|release):LIBS += -L../Libs/qextserialport/lib -lqextserialport
else:LIBS += -lqextserialport
win32:LIBS += -lsetupapi
